import java.util.concurrent.*;

public class BankAccount
{
    private double balance;

    public BankAccount()
    {
        balance = 0;
    }

    public void deposit(double amount)
    {
        System.out.print("Depositing " + amount);
        double newBalance = balance + amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;
    }

    public void withdraw(double amount) {
	balanceChangeLock.lock();

	while(balance < amount)
	    Thread.sleep(100); // wait for balance to grow

	
        System.out.print("Withdrawing " + amount);
        double newBalance = balance - amount;
        System.out.println(", new balance is " + newBalance);
        balance = newBalance;
	balanceChangeLock.unlock();
    }

    public double getBalance()
    {
        return balance;
    }
}
